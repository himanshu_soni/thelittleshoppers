//
//  ViewController.swift
//  TestProject
//
//  Created by Apple on 22/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
struct CategoryMain
{
    let label:String
    var items:[Category] = []
    init(label:String,items:[Category])
    {
        self.label = label
        self.items = items
    }
    
}


struct Category
{
    var category:String
    var imgUrl:String
    var motherCareItems:String
    var vegItems:[VegetablesDetails]
    
}

struct VegetablesDetails
{
    var vegName:String
    var vegCat:String
    var VegImg:String
    
}

struct SubCatMomsAndMaternity
{
    var image:String
    var name:String
    
}

class CategoryViewController: UIViewController
{
    
    @IBOutlet weak var catView: UIView!
    @IBOutlet weak var tblView: UITableView!
    var arrOfData:[CategoryMain] = []
    var arrCategory:[Category] = []
    var selectedRows:[IndexPath] = []
    var arrSubCatMomsAndMaternity:[SubCatMomsAndMaternity] = []
    
    let cellForHeight:CGFloat = 100
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationBarShadow()
        getDummyData()
       // subCategoryMomsMaternity()
        
       catView.layer.shadowColor = UIColor.black.cgColor
       catView.layer.shadowOpacity = 0.20
       catView.layer.shadowOffset = .zero
       catView.layer.shadowRadius = 5
        
    }
    
    func navigationBarShadow()
    {
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.5
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 5
    }
    
    
    
    
    //MARK:- Subcategory for moms and maternity
    
//    func subCategoryMomsMaternity()
//       {
//           let parameters = ["name":"m"]
//           AF.request("https://thelittleshoppers.com/API/searchSubcategory.php", method: .post, parameters: parameters).responseJSON{
//               response in
//            let result = response.value as! NSDictionary
//            let data = result["data"]as! [[String:Any]]
//
//            for details in data
//            {
//                let name = details["name"]as! String
//                let images = details["image"]as! String
//
//                let detailObj = SubCatMomsAndMaternity(image: images, name: name)
//                self.arrSubCatMomsAndMaternity.append(detailObj)
//
//
//            }
//            print(self.arrSubCatMomsAndMaternity)
//
//
//           }
//       }
    
    func getDummyData()
    {
        
        let veg1 = VegetablesDetails(vegName: "Cucumbers", vegCat: "Apple", VegImg: "cucumbers")
        let veg2 = VegetablesDetails(vegName: "Broccoli", vegCat: "Apple", VegImg: "Broccoli")
        let veg3 = VegetablesDetails(vegName: "Acorn squash", vegCat: "Apple", VegImg: "Acorn squash")
        let veg4 = VegetablesDetails(vegName: "Amaranth", vegCat: "Apricot", VegImg: "Amaranth")
        let veg5 = VegetablesDetails(vegName: "Bitter Gourds", vegCat: "Avocado", VegImg: "Bitter Gourds")
        let veg6 = VegetablesDetails(vegName: "Cardoon", vegCat: "Avocado", VegImg: "Cardoon")
        let veg7 = VegetablesDetails(vegName: "BottleGourd", vegCat: "Black Currant", VegImg: "Bottle-Gourd-Vegetable")
        
        let veg8 = VegetablesDetails(vegName: "Coconut", vegCat: "Black Currant", VegImg: "Coconut")
        let veg9 = VegetablesDetails(vegName: "Corn", vegCat: "Kiwi", VegImg: "corn")
        let veg10 = VegetablesDetails(vegName: "Cauliflower", vegCat: "Kiwi", VegImg: "cauliflower")
        

        
        let item1 = Category(category: "Moms And Maternity", imgUrl: "apple", motherCareItems: "Apple", vegItems: [veg1,veg2,veg3])
        let item2 = Category(category: "New Born Baby", imgUrl: "Apricot", motherCareItems: "Apricot", vegItems: [veg4,veg5])
        let item3 = Category(category: "Baby Boy", imgUrl: "Avocado", motherCareItems: "Avocado", vegItems: [veg6,veg7])
        let item4 = Category(category: "Baby Girl", imgUrl: "Black Currant", motherCareItems: "Black Currant", vegItems: [veg7,veg8])
        let item5 = Category(category: "Footwear", imgUrl: "Berry", motherCareItems: "Berry", vegItems: [])
        let item6 = Category(category: "Baby Wear", imgUrl: "Custard Apple", motherCareItems: "Custard Apple", vegItems: [])
        let item7 = Category(category: "Kids Wear", imgUrl: "Cherry", motherCareItems: "Cherry", vegItems: [])
        let item8 = Category(category: "Baby And Kids Necessity", imgUrl: "Kiwi", motherCareItems: "Kiwi", vegItems: [veg9,veg10])
        let item9 = Category(category: "Feeding And Nurshing", imgUrl: "Guava", motherCareItems: "Guava", vegItems: [])
        let item10 = Category(category: "Health And Safety", imgUrl: "Pomegranate", motherCareItems: "Pomegranate", vegItems: [])
        let item11 = Category(category: "Diapering", imgUrl: "Strawberry", motherCareItems: "Strawberry", vegItems: [])
        let item12 = Category(category: "Geor And Nursery", imgUrl: "Java plum", motherCareItems: "Java plum", vegItems: [])
        let item13 = Category(category: "Bath And Skin Care", imgUrl: "Mulberry", motherCareItems: "Mulberry", vegItems: [])
        
        let arrMain1 = CategoryMain(label: "Moms And Maternity", items: [item1,item2,item3,item4,item5,item6,item7,item8,item9,item10,item11,item12,item13])
        let arrMain2 = CategoryMain(label: "New Born Baby", items: [item2])
        let arrMain3 = CategoryMain(label: "Baby Boy", items: [item3])
        let arrMain4 = CategoryMain(label: "Baby Girl", items: [item4])
        let arrMain5 = CategoryMain(label: "Footwear", items: [item5])
        let arrMain6 = CategoryMain(label: "Baby Wear", items: [item6])
        let arrMain7 = CategoryMain(label: "Kids Wear", items: [item7])
        let arrMain8 = CategoryMain(label: "Baby And Kids Necessity", items: [item8])
        let arrMain9 = CategoryMain(label: "Feeding And Nurshing", items: [item9])
        let arrMain10 = CategoryMain(label: "Health And Safety", items: [item10])
        let arrMain11 = CategoryMain(label: "Diapering", items: [item11])
        let arrMain12 = CategoryMain(label: "Geor And Nursery", items: [item12])
        let arrMain13 = CategoryMain(label: "Bath And Skin Care", items: [item13])
        
      

        arrOfData = [arrMain1,arrMain2,arrMain3,arrMain4,arrMain5,arrMain6,arrMain7,arrMain8,arrMain9,arrMain10,arrMain11,arrMain12,arrMain13]
    }


}

extension CategoryViewController:UITableViewDelegate,UITableViewDataSource
{
//    func numberOfSections(in tableView: UITableView) -> Int
//    {
//
//          return arrOfData.count
//      }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrOfData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemsTableViewCell", for: indexPath)as! ItemsTableViewCell
        cell.contentView.backgroundColor = UIColor.white
        cell.contentView.layer.borderWidth = 0.2
        cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
           cell.layer.shadowColor = UIColor.black.cgColor
           cell.layer.shadowRadius = 5

           cell.layer.shadowOpacity = 0.0
           cell.layer.masksToBounds = false;
           cell.clipsToBounds = false;
        
        cell.arrData = arrOfData
        cell.items = arrOfData[indexPath.row].items
        
        cell.categoryLbl.text = arrOfData[indexPath.row].label
       
        cell.btnObj.addTarget(self, action: #selector(checkMarkedBtn(sender:)), for: .touchUpInside)
               if selectedRows.contains(indexPath)
                 {
                   cell.btnObj.setImage(UIImage(named:"Minus"), for: .normal)
                    let height = cell.motherCareTblView.contentSize.height
                          // print("height",height)
                    cell.heightConstraint.constant = height
                 }
                 else
                 {
                   cell.btnObj.setImage(UIImage(named:"Plus"), for: .normal)
                    cell.heightConstraint.constant = 0
               }
            
        cell.btnObj.tag = indexPath.row
        
        
        
        
        cell.parent = self
        return cell
        
        
    }
    
    
    
    
    @objc func checkMarkedBtn(sender : UIButton)
    {
        
        
        let selectedIndexPath = IndexPath(row: sender.tag, section: 0)
        
        print(selectedIndexPath)
        
        if self.selectedRows.contains(selectedIndexPath)
        {
            self.selectedRows.remove(at: self.selectedRows.firstIndex(of: selectedIndexPath)!)
        }
        else
        {
          self.selectedRows.append(selectedIndexPath)
            
            print("Selected rows",selectedRows)
           
        }
        tblView.reloadData()


    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
      
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return cellForHeight
    }
   
    
}
