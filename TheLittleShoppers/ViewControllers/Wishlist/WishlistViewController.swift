//
//  WishlistViewController.swift
//  TestProject
//
//  Created by Pushkar Raj Chauhan on 13/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import MBProgressHUD
import Toast_Swift
class WishlistViewController: UIViewController {
    
    @IBOutlet weak var collectionView:UICollectionView!
    let viewModel = WishlistViewModel()
    var wishlistData = [WishlistData]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func loadView() {
        super.loadView()
        self.view.showHUD()
        viewModel.getWishlistData { (data, error) in
            self.view.hideHUD()
            if error == nil{
                self.collectionView.delegate = self
                self.collectionView.dataSource = self
                self.collectionView.reloadData()
            }else{
                self.view.showToastMessage(message: error!.desc)
            }
        }
    }
}
extension WishlistViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (viewModel.wishlistData?.wishlistData.count)!
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WishlistCollectionViewCell", for: indexPath) as! WishlistCollectionViewCell
        let data = (viewModel.wishlistData?.wishlistData[indexPath.row])!
        cell.itemNameLabel.text = data.productName
        cell.discounpriceLabel.text = data.discountedSalePrice
        cell.realPriceLabel.text = data.regularPrice
        cell.discountPercentageLabel.text = data.discount
        let image = data.image
        let url = URL(string: image)
        cell.previewImageView!.sd_setImage(with: url, placeholderImage: UIImage(named: "1"), options: SDWebImageOptions.highPriority, context: nil)
        cell.layer.cornerRadius = 5.0
        cell.layer.borderWidth = 0.0
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.layer.shadowRadius = 3.0
        cell.layer.shadowOpacity = 1
        cell.layer.masksToBounds = false //<-
       
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionView{
            return CGSize(width: (self.collectionView.frame.size.width / 2) - 15, height: 270)
        }
        else{
            return CGSize(width: 0.00, height: 0.00)
        }
    }
}
