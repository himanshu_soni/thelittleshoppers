//
//  NextViewController.swift
//  TestProject
//
//  Created by Apple on 22/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class NextViewController: UIViewController
{

    @IBOutlet weak var heightVariable: NSLayoutConstraint!
    var arrData:[CategoryMain]?
    var items:[Category]?
    var vegItems:[VegetablesDetails]?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NextViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return vegItems!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCollectionViewCell", for: indexPath) as! ImagesCollectionViewCell
        
       
        cell.imgView.image = UIImage(named: "\(vegItems![indexPath.row].VegImg)")
        cell.nameLbl.text = vegItems![indexPath.row].vegName
        return cell
        
    }
    
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
       {
         
         let layout = collectionViewLayout as! UICollectionViewFlowLayout
          layout.minimumLineSpacing = 5
          layout.minimumInteritemSpacing = 5
                          
                          let numberOfItemsPerRow: CGFloat = 2
                          let itemWidth = (collectionView.bounds.width - layout.minimumLineSpacing) / numberOfItemsPerRow
                          
                          return CGSize(width: itemWidth, height: 150)
         
       }
    
    
}
