//
//  TabBarViewController.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 15/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().tintColor =  #colorLiteral(red: 0, green: 0.2470588235, blue: 0.4901960784, alpha: 1)
        UITabBar.appearance().unselectedItemTintColor = #colorLiteral(red: 0.6666666667, green: 0.7176470588, blue: 0.7882352941, alpha: 1)
        setDataOntheView()
        self.navigationController?.navigationBar.isHidden = true
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.selectedIndex = 2
    }
    
    @objc func setDataOntheView() {
        let categoriesVc = UINavigationController(rootViewController: CategoryViewController.instantiate(fromAppStoryboard: .Categories))
        let wishlistVC = UINavigationController(rootViewController: WishlistViewController.instantiate(fromAppStoryboard: .Wishlist))
        let homeVC = UINavigationController(rootViewController: HomeViewController.instantiate(fromAppStoryboard: .Home))
        let accountVc = UINavigationController(rootViewController: AccountViewController.instantiate(fromAppStoryboard: .Account))
        let cartVc = UINavigationController(rootViewController: CartViewController.instantiate(fromAppStoryboard: .Cart))
        
        
        let categoryBarItem = UITabBarItem(title: "Category", image: UIImage(named: ""), selectedImage: nil)
        let wishlistBarItem = UITabBarItem(title: "Wishlist", image: UIImage(named: ""), selectedImage: nil)
        let homeBarItem = UITabBarItem(title: "Home", image: UIImage(named: ""), selectedImage: nil)
        let accountBarItem = UITabBarItem(title: "Account", image: UIImage(named: ""), selectedImage: nil)
        let cartBarItem = UITabBarItem(title: "Cart", image: UIImage(named: ""), selectedImage: nil)
        
        categoriesVc.tabBarItem = categoryBarItem
        wishlistVC.tabBarItem = wishlistBarItem
        homeVC.tabBarItem = homeBarItem
        accountVc.tabBarItem = accountBarItem
        cartVc.tabBarItem = cartBarItem
        
        self.viewControllers = [categoriesVc , wishlistVC, homeVC, accountVc, cartVc]
        selectedIndex = 2
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
