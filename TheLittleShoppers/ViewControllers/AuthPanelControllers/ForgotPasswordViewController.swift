//
//  ForgotPasswordViewController.swift
//  TestProject
//
//  Created by Apple on 01/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
//import SwiftyJSON
class ForgotPasswordViewController: UIViewController
{

    @IBOutlet weak var mobileField: UITextField!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        mobileField.leftViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: "phone")
        imageView.image = image
        mobileField.leftView = imageView
    }
    
    
    func getOtp()
    {
        let phoneNo = mobileField.text!
        
        let parameters = ["phone":"\(phoneNo)"]
//        AF.request("https://thelittleshoppers.com/API/forgetPassword.php", method: .post, parameters: parameters).responseJSON{
//            response in
//           let result = response.value as! NSDictionary
//           let data = result["message"]as! String
//          let getOtp = result["otp"]as! String
//        print(response,data,getOtp)
//
//        }
    }

   
    @IBAction func sendOtpAction(_ sender: Any)
    {
        let phoneNo = mobileField.text!
        if phoneNo.isEmpty
        {
            display_Alert(appName: "Rooprang shopping Application", msg: "Please enter mobile no.", viewCon: self)
            return
        }
        else
        {
        getOtp()
        }
        
    }
    
    func display_Alert(appName:String,msg:String,viewCon:UIViewController)
    {
       let alertVC = UIAlertController(title: appName, message: msg, preferredStyle: .alert)
       let OkBtn = UIAlertAction(title: "Ok", style: .default, handler: nil)
       alertVC.addAction(OkBtn)
        viewCon.present(alertVC, animated: true, completion: nil)
    }
    
}
