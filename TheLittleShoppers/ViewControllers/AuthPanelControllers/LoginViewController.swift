//
//  LoginViewController.swift
//  TestProject
//
//  Created by Apple on 28/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
//import SwiftyJSON
import Alamofire
class LoginViewController: UIViewController
{
    let viewModel = LoginViewModel()
    @IBOutlet weak var emailTextField:UITextField!
    @IBOutlet weak var passwordTextField:UITextField!

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    override func loadView() {
        super.loadView()
        let vc = TabBarViewController.instantiate(fromAppStoryboard: .TabBarManager) as! TabBarViewController
        UIApplication.shared.windows.first?.rootViewController = UINavigationController(rootViewController: vc)
    }
}
//MARK: Action Methods.
extension LoginViewController{
   @IBAction func signUpAction(_ sender: Any)
    {
        let signUp = self.storyboard?.instantiateViewController(identifier: "SignUpViewController")as! SignUpViewController
        self.present(signUp, animated: true, completion: nil)
    }
    @IBAction func loginAction(_ sender: Any){
//        if emailTextField.text!.isEmpty{
//            print("email Field Should not be left empty")
//        }else if passwordTextField.text!.isEmpty{
//            print("passWord Field Should not be left empty")
//        }else{
//            viewModel.loginApiCall { (data, error) in
//                print(data)
//            }
            let vc = TabBarViewController.instantiate(fromAppStoryboard: .TabBarManager) as! TabBarViewController
            UIApplication.shared.windows.first?.rootViewController = UINavigationController(rootViewController: vc)
//        }
    }
    
    @IBAction func forgotPassword(_ sender: Any)
    {
        let forgotVc = self.storyboard?.instantiateViewController(identifier: "ForgotPasswordViewController")as! ForgotPasswordViewController
        self.present(forgotVc, animated: true, completion: nil)
        
    }
    
    
    
    
}
