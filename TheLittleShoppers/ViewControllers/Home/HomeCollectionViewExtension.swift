//
//  HomeCollectionViewExtension.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 22/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        let data = viewModel.homeObject?.homeData
        switch collectionView{
        case pagerCollectionView:
            return (data?.pagerData.count)!
        case topCollectionView:
            return (data?.shopBycategory.count)!
        case dealofDayCollectionView:
            //            return 5
            return (data?.dealOftheDay?.dealOfDayData.count)!
        case shopByCategoryCollectionView:
            return (data?.shopBycategory.count)!
        case bloomByStyleCollectionView:
            return (data?.bloomStyleData.count)!
        case allHomeCollectionView:
            return (data?.allHomeBannerData.count)!
        case trendingCollectionView:
            //            return 5
            return (data?.bloomStyleData.count)!
        case latestCollectionView:
            return (data?.latestProductData.count)!
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        switch collectionView{
        case pagerCollectionView:
            let data = self.viewModel.homeObject?.homeData?.pagerData[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PagingCollectionViewCell", for: indexPath) as! PagingCollectionViewCell
            let url = URL(string: data!.bannerImageUrl)
            cell.previewImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "1"), options: SDWebImageOptions.highPriority, completed: nil)
            return cell
        case topCollectionView:
            let cell:MomsProductCollectionViewCell = self.topCollectionView.dequeueReusableCell(withReuseIdentifier:"MomsProductCollectionViewCell", for: indexPath) as! MomsProductCollectionViewCell
            let data = self.viewModel.homeObject?.homeData?.shopBycategory[indexPath.row]
            cell.nameLbl.text = data?.subcategoryName
            let url = URL(string: data!.subcategoryImage)
            cell.imgView.sd_setImage(with: url, placeholderImage: UIImage(named: "1"), options: SDWebImageOptions.highPriority, completed: nil)
            return cell
        case dealofDayCollectionView:
            let data = self.viewModel.homeObject?.homeData?.dealOftheDay?.dealOfDayData[indexPath.row]
            let cell = dealofDayCollectionView.dequeueReusableCell(withReuseIdentifier:"DealOfDayCollectionViewCell", for: indexPath) as! DealOfDayCollectionViewCell
            cell.productNameLbl.text = data?.productName
            cell.discountLbl.text = data?.productName
            cell.mrpLbl.text = "300"
            cell.imgView.backgroundColor = .systemPink
            self.dealCollectionViewHeight.constant = dealofDayCollectionView.contentSize.height
            self.dealofDayCollectionView.layoutIfNeeded()
            return cell
        case shopByCategoryCollectionView:
            let data = self.viewModel.homeObject?.homeData?.shopBycategory[indexPath.row]
            let cell = shopByCategoryCollectionView.dequeueReusableCell(withReuseIdentifier: "ShopByCollectionViewCell", for: indexPath) as! ShopByCollectionViewCell
            cell.nameLbl.text = data?.subcategoryName
            let url = URL(string: data!.subcategoryImage)
            cell.imgView.sd_setImage(with: url, placeholderImage: UIImage(named: "1"), options: SDWebImageOptions.highPriority, completed: nil)
            self.shopCategoryCollectionViewHeight.constant = shopByCategoryCollectionView.contentSize.height
            self.shopByCategoryCollectionView.layoutIfNeeded()
            return cell
        case bloomByStyleCollectionView:
            let cell = self.bloomByStyleCollectionView.dequeueReusableCell(withReuseIdentifier: "BloomCollectionViewCell", for: indexPath) as! BloomCollectionViewCell
            let data = self.viewModel.homeObject?.homeData?.bloomStyleData[indexPath.row]
            let url = URL(string: (data?.subCategoryImage)!)
            cell.imgView!.sd_setImage(with: url, placeholderImage: UIImage(named: "1"), options: SDWebImageOptions.highPriority, completed: nil)
            cell.nameLbl.text = data?.SubcategoryName
            self.bloomInStyleCollectionViewHeight.constant = bloomByStyleCollectionView.contentSize.height
            self.bloomByStyleCollectionView.layoutIfNeeded()
            return cell
        case allHomeCollectionView:
            let data = self.viewModel.homeObject?.homeData?.allHomeBannerData[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllHomeCollectionViewCell", for: indexPath) as! AllHomeCollectionViewCell
            let url = URL(string: data!.bannerImage)
            cell.offerNameLabel.text = data?.bannerName
            cell.previewImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "1"), options: SDWebImageOptions.highPriority, completed: nil)
            self.allHomeCollectionViewHeight.constant = collectionView.contentSize.height
            self.allHomeCollectionView.layoutIfNeeded()
            return cell
        case trendingCollectionView:
//            let data = self.viewModel.homeObject?.homeData?.trendingData?.trendingData[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingCollectionViewCell", for: indexPath) as! TrendingCollectionViewCell
            cell.nameLbl.text = "Product Name Here"
            cell.imgView.backgroundColor = .brown
            self.trendingCollectionViewHeight.constant = trendingCollectionView.contentSize.height
            self.trendingCollectionView.layoutIfNeeded()
            return cell
        case latestCollectionView:
            let data = self.viewModel.homeObject?.homeData?.latestProductData[indexPath.row]
            let cell = latestCollectionView.dequeueReusableCell(withReuseIdentifier: "LatestProductCollectionViewCell", for: indexPath) as! LatestProductCollectionViewCell
            cell.roundTopCorners(radius: 5)
            cell.roundBottomCorners(radius: 50)
            cell.nameLbl.text = data?.productName
            cell.priceLbl.text = data?.salePrice
            let url = URL(string: data!.image)
            cell.imgView.sd_setImage(with: url, placeholderImage: UIImage(named: ""), options: SDWebImageOptions.highPriority, completed: nil)
            self.latestProductsColllectionViewHeight.constant = latestCollectionView.contentSize.height
            self.latestCollectionView.layoutIfNeeded()
            return cell
        default:
            return UICollectionViewCell.init()
        }
        //return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let vc  = ProductDetailViewController.instantiate(fromAppStoryboard: .Home) as! ProductDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        switch collectionView{
        case pagerCollectionView:
            return CGSize(width: pagerCollectionView.bounds.width - 20, height: 250)
        case topCollectionView:
            let width = topCollectionView.frame.width/4 - 5
            return CGSize(width: width , height: 135)
        case dealofDayCollectionView:
            return CGSize(width: (self.dealofDayCollectionView.frame.size.width - 30 )/2, height: 260)
        case shopByCategoryCollectionView:
            return CGSize(width: (self.shopByCategoryCollectionView.frame.size.width - 30 )/2, height: 150.00)
        case bloomByStyleCollectionView:
            return CGSize(width: (self.bloomByStyleCollectionView.frame.size.width - 30 )/2, height: 135.00)
        case allHomeCollectionView:
            return CGSize(width: allHomeCollectionView.bounds.width - 20, height: 350)
        case trendingCollectionView:
            return CGSize(width: (self.trendingCollectionView.frame.size.width - 30 )/2, height: self.trendingCollectionView.frame.size.height - 12)
        case latestCollectionView:
            return CGSize(width: (self.latestCollectionView.frame.size.width - 30 )/2, height: 240.00)
        default:
            return CGSize(width: 0, height: 0)
        }
    }
}
