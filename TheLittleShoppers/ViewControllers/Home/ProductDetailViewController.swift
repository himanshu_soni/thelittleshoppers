//
//  ProductDetailViewController.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 21/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController {
    @IBOutlet weak var pagingCollectionView:UICollectionView!
    @IBOutlet weak var similarCollectionView:UICollectionView!
    @IBOutlet weak var productDetailLabel:UILabel!
    @IBOutlet weak var productDetailLabel2:UILabel!
    @IBOutlet weak var discountLabel:UILabel!
    @IBOutlet weak var realPriceLabel:UILabel!
    @IBOutlet weak var discountPercentageLabel:UILabel!
    @IBOutlet weak var ratinglabelLabel:UILabel!
    @IBOutlet weak var productCountTextField:UITextField!
    @IBOutlet weak var pincodeTextField:UITextField!
    @IBOutlet weak var productDetailsLabel:UILabel!
    @IBOutlet weak var pageControl:UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pagingCollectionView.decelerationRate = .fast
        // Do any additional setup after loading the view.
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       if scrollView == pagingCollectionView{
        let pageIndex = round(scrollView.contentOffset.x/scrollView.frame.size.width)
            pageControl.currentPage = Int(pageIndex)
        }
    }
}

//MARK:IBActions are handled here.
extension ProductDetailViewController{
    @IBAction func ratingButtonPressed(sender:UIButton){
        
        
        
    }
    @IBAction func minusPressed(sender:UIButton){
        
        
        
    }
    @IBAction func plusButtonPressed(sender:UIButton){
        
    }
    
    @IBAction func pincodeButtonPressed(sender:UIButton){
        
    }
    
    
}


extension ProductDetailViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == similarCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SimilarCollectionViewCell", for: indexPath) as! SimilarCollectionViewCell
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PagingCollectionViewCell", for: indexPath) as! PagingCollectionViewCell
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == similarCollectionView{
            return CGSize(width: (similarCollectionView.bounds.width - 20)/2, height: 170)
        }else{
            return CGSize(width: pagingCollectionView.bounds.width - 15, height: pagingCollectionView.bounds.height)
        }
    }
}
