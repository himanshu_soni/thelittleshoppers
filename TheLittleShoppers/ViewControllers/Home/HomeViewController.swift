//
//  HomeViewController.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 22/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SDWebImage
class HomeViewController: UIViewController
{
    @IBOutlet weak var pagerView: UIView!
    @IBOutlet weak var dealOfDayView: UIView!
    @IBOutlet weak var shopByCategoryView: UIView!
    @IBOutlet weak var allHomeBannerView: UIView!
    @IBOutlet weak var bloomByStyleView: UIView!
    @IBOutlet weak var trendingView: UIView!
    @IBOutlet weak var latestProductsView: UIView!
    
    @IBOutlet weak var topCollectionView: UICollectionView!
    @IBOutlet weak var pagerCollectionView: UICollectionView!
    @IBOutlet weak var dealofDayCollectionView: UICollectionView!
    @IBOutlet weak var shopByCategoryCollectionView: UICollectionView!
    @IBOutlet weak var allHomeCollectionView: UICollectionView!
    @IBOutlet weak var bloomByStyleCollectionView: UICollectionView!
    @IBOutlet weak var trendingCollectionView: UICollectionView!
    @IBOutlet weak var latestCollectionView: UICollectionView!
    
    @IBOutlet weak var dealCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var shopCategoryCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bloomInStyleCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var trendingCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var latestProductsColllectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var allHomeCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var myPageControl: UIPageControl!
    
    let viewModel = HomeViewModel()
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    override func loadView() {
        super.loadView()
        refreshPage()
//        self.trendingCollectionView.delegate = self
//        self.trendingCollectionView.dataSource = self
//        self.dealofDayCollectionView.delegate = self
//        self.dealofDayCollectionView.dataSource = self
        pagerCollectionView.decelerationRate = .fast
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == pagerCollectionView{
            let pageIndex = round(scrollView.contentOffset.x/scrollView.frame.size.width)
            myPageControl.currentPage = Int(pageIndex)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshPage()
    }
    fileprivate func refreshPage(){
        self.pagerView.isHidden = true
        self.dealOfDayView.isHidden = true
        self.shopByCategoryView.isHidden = true
        self.allHomeBannerView.isHidden = true
        self.bloomByStyleView.isHidden = true
        self.trendingView.isHidden = true
        self.latestProductsView.isHidden = true
        self.view.showHUD()
        viewModel.getHomeData1 { (data, error) in
            self.view.hideHUD()
            if error == nil{
                self.topCollectionView.isHidden = false
                self.topCollectionView.delegate = self
                self.topCollectionView.dataSource = self
                self.topCollectionView.reloadData()
                let data = self.viewModel.homeObject?.homeData
                if data?.pagerData.count != 0{
                    self.myPageControl.numberOfPages = (data?.pagerData.count)!
                    self.pagerView.isHidden = false
                    self.pagerCollectionView.delegate = self
                    self.pagerCollectionView.dataSource = self
                    self.pagerCollectionView.reloadData()
                }
                if data?.shopBycategory.count != 0{
                    self.shopByCategoryView.isHidden = false
                    self.shopByCategoryCollectionView.delegate = self
                    self.shopByCategoryCollectionView.dataSource = self
                    self.shopByCategoryCollectionView.reloadData()
                }
                if data?.dealOftheDay?.dealOfDayData.count != 0{
                    self.dealOfDayView.isHidden = false
                    self.dealofDayCollectionView.delegate = self
                    self.dealofDayCollectionView.dataSource = self
                    self.dealofDayCollectionView.reloadData()
                }
                if data?.allHomeBannerData.count != 0{
                    self.allHomeBannerView.isHidden = false
                    self.allHomeCollectionView.delegate = self
                    self.allHomeCollectionView.dataSource = self
                    self.allHomeCollectionView.reloadData()
                }
                if data?.bloomStyleData.count != 0{
                    self.bloomByStyleView.isHidden = false
                    self.bloomByStyleCollectionView.delegate = self
                    self.bloomByStyleCollectionView.dataSource = self
                    self.bloomByStyleCollectionView.reloadData()
                    self.trendingCollectionView.delegate = self
                    self.trendingCollectionView.dataSource = self
                    self.trendingCollectionView.reloadData()
                }
                if data?.trendingData?.trendingData.count != 0{
                    self.trendingView.isHidden = false
                    self.trendingCollectionView.delegate = self
                    self.trendingCollectionView.dataSource = self
                    self.trendingCollectionView.reloadData()
                    self.bloomByStyleView.isHidden = false
                    self.bloomByStyleCollectionView.delegate = self
                    self.bloomByStyleCollectionView.dataSource = self
                    self.bloomByStyleCollectionView.reloadData()
                }
                if data?.latestProductData.count != 0{
                    self.latestProductsView.isHidden = false
                    self.latestCollectionView.delegate = self
                    self.latestCollectionView.dataSource = self
                    self.latestCollectionView.reloadData()
                }else{
                    self.view.showToastMessage(message: "NO Data Fetched")
                }
                self.view.showToastMessage(message: "data fetched from home api")
            }else{
                self.view.showToastMessage(message: error!.desc)
            }
        }
    }
}

