//
//  CartViewController.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 15/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
    
    let viewModel = CartViewModel()
    var cartData = [CartData]()

    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func loadView() {
        super.loadView()
        self.view.showHUD()
        viewModel.getCartData { (data, error) in
            self.view.hideHUD()
            if error == nil{
                self.myTableView.delegate = self
                self.myTableView.dataSource = self
                self.myTableView.reloadData()
            }else{
                self.view.showToastMessage(message: error!.desc)
            }
        }
    }
}
extension CartViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let vc = self.myTableView.dequeueReusableCell(withIdentifier: "CartTableViewCell") as! CartTableViewCell
//        let data = (viewModel.cartData?.cartData[indexPath.row])!
//       // let data = cartData[indexPath.row]
//        print(data)
//        vc.lblProductName.text = data.productName
//        
        return vc
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           print(indexPath.row)
       }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.00
       }
    
}
