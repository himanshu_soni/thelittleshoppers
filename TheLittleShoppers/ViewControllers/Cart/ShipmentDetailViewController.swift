
//
//  ShipmentDetailViewController.swift
//  TestProject
//
//  Created by Pushkar Raj Chauhan on 12/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ShipmentDetailViewController: UIViewController {
    @IBOutlet weak var stateLabel:UILabel!
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var paymentMethodLabel:UILabel!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var fullAddressLabel:UILabel!
    @IBOutlet weak var landMarkLabel:UILabel!
    @IBOutlet weak var cityLabel:UILabel!
    @IBOutlet weak var pradeshLabel:UILabel!
    @IBOutlet weak var pincodeLabel:UILabel!
    @IBOutlet weak var totalItemPrice:UILabel!
    @IBOutlet weak var discountLabel:UILabel!
    @IBOutlet weak var totalLabel:UILabel!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
}
extension ShipmentDetailViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShipmentDetailTableViewCell") as! ShipmentDetailTableViewCell
        cell.cancelButtonPressed = {
            print(indexPath.row)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        self.tableViewHeight.constant = tableView.contentSize.height
        self.tableView.layoutIfNeeded()
        return UITableView.automaticDimension
    }
}
