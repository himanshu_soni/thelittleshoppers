//
//  UpdateProfileViewController.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 19/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import UIKit

class UpdateProfileViewController: UIViewController {
    @IBOutlet weak var profileImageView:UIImageView!
    @IBOutlet weak var nameTextField:UITextField!
    @IBOutlet weak var mobileTextField:UITextField!
    @IBOutlet weak var emailTextField:UITextField!
    @IBOutlet weak var changePasswordField:UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.setIcon(UIImage(named: "Contact")!)
        emailTextField.setIcon(UIImage(named: "Message")!)
        mobileTextField.setIcon(UIImage(named: "Phone")!)
        changePasswordField.setIcon(UIImage(named: "Lock")!)
        self.setRightIcon(nameTextField, UIImage(named: "Pen")!)
        // Do any additional setup after loading the view.
    }
    
    func setRightIcon(_ textField:UITextField,_ image:UIImage){
            let icon = UIButton(frame:
            CGRect(x: 0, y: 0, width: 30, height: 30))
            icon.setImage(image, for: .normal)
    //        let iconView = UIImageView(frame:
    //            CGRect(x: 0, y: 0, width: 30, height: 30))
    //        iconView.image = image
           icon.addTarget(self, action: #selector(rightViewButtonTapped(_:)), for: .touchUpInside)
            let iconContainerView: UIView = UIView(frame:
                CGRect(x: 10, y: 0, width: 36, height: 30))
            iconContainerView.addSubview(icon)
           textField.rightView = iconContainerView
            textField.rightViewMode = .always
        }
        @objc func rightViewButtonTapped(_ textField:UITextField){
            textField.isUserInteractionEnabled = true
            //self.nameTextField.isEditing = true
           print("right View Button Tapped")
        }
}


extension UpdateProfileViewController{
    @IBAction func addProfilePhoto(sender:UIButton){
        
        
    }
    @objc func enterEditingMode(sender:UIButton){
        
        
    }
    
}
