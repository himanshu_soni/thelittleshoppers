//
//  AccountViewController.swift
//  TestProject
//
//  Created by Apple on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

struct AccountDetails{
    let accountImg:String
    let accHistory:String
}

class AccountViewController: UIViewController{
    @IBOutlet weak var heightVariable: NSLayoutConstraint!
    @IBOutlet weak var helpButton: UIButton!

    @IBOutlet weak var feedbackButton: UIButton!

    @IBOutlet weak var shareButton: UIButton!

    @IBOutlet weak var ratingButton: UIButton!

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var accountView: UIView!
    var arrAccountDetails:[AccountDetails] = []
    override func viewDidLoad(){
        super.viewDidLoad()
//        shareButton.alignImageAndTitleVertically()
//        feedbackButton.alignImageAndTitleVertically()
//        ratingButton.alignImageAndTitleVertically()
//        helpButton.alignImageAndTitleVertically()
//        tblView.estimatedRowHeight = self.tblView.rowHeight
//        tblView.rowHeight = UITableView.automaticDimension
//        accountView.layer.shadowColor = UIColor.black.cgColor
//        accountView.layer.shadowOpacity = 0.10
//        accountView.layer.shadowOffset = .zero
//        accountView.layer.shadowRadius = 5
        accountHistory()
    }
    func accountHistory(){
        let a1 = AccountDetails(accountImg: "", accHistory: "Your Orders")
        let a2 = AccountDetails(accountImg: "", accHistory: "Wishlist")
        let a3 = AccountDetails(accountImg: "", accHistory: "Profile Details")
        let a4 = AccountDetails(accountImg: "", accHistory: "Saved Addresses")
        arrAccountDetails = [a1,a2,a3,a4]
    }
}
extension AccountViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrAccountDetails.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountDetailsTableViewCell", for: indexPath)as! AccountDetailsTableViewCell
        cell.detailLbl.text = arrAccountDetails[indexPath.row].accHistory
//        self.heightVariable.constant = tblView.contentSize.height
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        switch indexPath.row{
        case 0 :
            let vc = self.storyboard?.instantiateViewController(identifier: "OrderDescriptionViewController") as! OrderDescriptionViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 1 :
            self.tabBarController?.selectedIndex = 1

        case 2 :
            print("2")
        case 3 :
            print("3")
        default :
            print("none is selected here.")
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }
}
