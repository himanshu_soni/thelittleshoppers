//
//  OrderDescriptionViewController.swift
//  TestProject
//
//  Created by Pushkar Raj Chauhan on 12/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class OrderDescriptionViewController: UIViewController {

    @IBOutlet weak var tableView:UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }


}

extension OrderDescriptionViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDescriptionTableViewCell") as! OrderDescriptionTableViewCell
        
        cell.clickOnViewOrder = {
            print(indexPath.row)
            let vc = self.storyboard?.instantiateViewController(identifier: "ShipmentDetailViewController") as! ShipmentDetailViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}



