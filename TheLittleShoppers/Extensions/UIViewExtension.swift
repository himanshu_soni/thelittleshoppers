//
//  UIViewExtension.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 15/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import UIKit
import MBProgressHUD
import Toast_Swift

extension UIView {
    /// A property that accesses the backing layer's masksToBounds.
    @IBInspectable
    open var masksToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set(value) {
            layer.masksToBounds = value
        }
        
        
    }
    
    
    /// A property that accesses the backing layer's opacity.
    @IBInspectable
    open var opacity: Float {
        get {
            return layer.opacity
        }
        set(value) {
            layer.opacity = value
        }
    }
    
    /// A property that accesses the backing layer's anchorPoint.
    @IBInspectable
    open var anchorPoint: CGPoint {
        get {
            return layer.anchorPoint
        }
        set(value) {
            layer.anchorPoint = value
        }
    }
    
    /// A property that accesses the backing layer's shadow
    @IBInspectable
    open var shadowColor: UIColor? {
        get {
            guard let v = layer.shadowColor else {
                return nil
            }
            
            return UIColor(cgColor: v)
        }
        set(value) {
            layer.shadowColor = value?.cgColor
        }
    }
    
    /// A property that accesses the backing layer's shadowOffset.
    @IBInspectable
    open var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set(value) {
            layer.shadowOffset = value
        }
    }
    
    /// A property that accesses the backing layer's shadowOpacity.
    @IBInspectable
    open var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set(value) {
            layer.shadowOpacity = value
        }
    }
    
    /// A property that accesses the backing layer's shadowRadius.
    @IBInspectable
    open var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set(value) {
            layer.shadowRadius = value
        }
    }
    
    /// A property that accesses the backing layer's shadowPath.
    @IBInspectable
    open var shadowPath: CGPath? {
        get {
            return layer.shadowPath
        }
        set(value) {
            layer.shadowPath = value
        }
    }
    
    /// A property that accesses the layer.cornerRadius.
    @IBInspectable
    open var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set(value) {
            layer.cornerRadius = value
        }
    }
    
    /// A property that accesses the layer.borderWith.
    @IBInspectable
    open var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set(value) {
            layer.borderWidth = value
        }
    }
    
    /// A property that accesses the layer.borderColor property.
    @IBInspectable
    open var borderColor: UIColor? {
        get {
            guard let v = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: v)
        }
        set(value) {
            layer.borderColor = value?.cgColor
        }
    }
    
    /// A property that accesses the layer.position property.
    @IBInspectable
    open var position: CGPoint {
        get {
            return layer.position
        }
        set(value) {
            layer.position = value
        }
    }
    
    /// Shows Darlingo progress view
    internal func showHUD() {
        MBProgressHUD.showAdded(to: self, animated: true)
    }
    
    /// Hides Darlingo progress view
    internal func hideHUD() {
        MBProgressHUD.hide(for: self, animated: false)
    }
    internal func showToastMessage(message:String) {
        var style = ToastStyle()
        style.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7188570205)
        self.makeToast(message, duration: 2.0, position: .bottom)
    }

}
extension UIView {
    
    func fadeIn(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
//            self.alpha = 1.0
            
            self.opacity = 1
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 1.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
//            self.alpha = 0.3
            self.opacity = 0.4
        }, completion: completion)
    }
}

extension UIView {

    func roundTopCorners( radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    func roundBottomCorners(radius:CGFloat){
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
}
extension UIView {
    
//    func loadInterruptionView(message: String = "")  -> ErrorView {
//        removeInterruptionViewFromSuperview()
//        let xib = Bundle.main.loadNibNamed(String(describing :"ErrorView"), owner: nil, options: nil)
//        let interruptView = xib![0] as! ErrorView
//        interruptView.message = message
//        
//        interruptView.frame = self.bounds
//        interruptView.center = self.center
//        interruptView.tag = 1001
//        if self is UITableView || self is UICollectionView {
//            //               self.superview?.insertSubview(interruptView, aboveSubview: self)
//            self.backgroundColor = .clear
//            self.superview?.insertSubview(interruptView, aboveSubview: self)
//        }else{
//            self.addSubview(interruptView)
//        }
//        return interruptView
//    }
//    
//    func loadFeedbackAlertView()  -> FeedBackAlertView {
//        removeFeedbackAlertView()
//        let xib = Bundle.main.loadNibNamed(String(describing :"View"), owner: nil, options: nil)
//        let interruptView = xib![0] as! FeedBackAlertView
//        interruptView.frame = self.bounds
//        interruptView.center = self.center
//        interruptView.tag = 1001
//        if self is UITableView || self is UICollectionView {
//            //               self.superview?.insertSubview(interruptView, aboveSubview: self)
//            self.backgroundColor = .clear
//            self.superview?.insertSubview(interruptView, aboveSubview: self)
//        }else{
//            self.addSubview(interruptView)
//        }
//        return interruptView
//    }
//    
//       func removeFeedbackAlertView() {
//           if self is UITableView || self is UICollectionView {
//               if let viewWithTag = self.superview?.viewWithTag(1001) {
//                   viewWithTag.removeFromSuperview()
//                   self.backgroundColor = .white
//               }else{
//                   print("No!")
//               }
//           }else {
//               if let viewWithTag = self.viewWithTag(1001) {
//                   viewWithTag.removeFromSuperview()
//               }else{
//                   print("No!")
//               }
//           }
//       }
//    
    func removeInterruptionViewFromSuperview() {
        
        if self is UITableView || self is UICollectionView {
            if let viewWithTag = self.superview?.viewWithTag(1001) {
                viewWithTag.removeFromSuperview()
                self.backgroundColor = .white
            }else{
                print("No!")
            }
        }else {
            if let viewWithTag = self.viewWithTag(1001) {
                viewWithTag.removeFromSuperview()
            }else{
                print("No!")
            }
        }
    }
   
    
}
extension UITextField {
    
    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:
            CGRect(x: 0, y: 0, width: 30, height: 30))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 10, y: 0, width: 36, height: 30))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
    }
   
    
}
