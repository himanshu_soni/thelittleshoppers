//
//  ViewControllerExtension.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 14/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    /// Shows an alert to user with a single button to dismiss the alert.
    ///
    /// - Parameters:
    ///   - title: Title of the alert
    ///   - messageToShow: Message to show in the alert
    ///   - buttonTitle: Button title to use. If not given "Ok" is assumed.
    internal func showAlert(title: String?, message messageToShow: String?, buttonTitle: String = "Ok") {
        let alert = UIAlertController(title: title, message: messageToShow, preferredStyle: UIAlertController.Style.alert)
        
        let defaultAction = UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(defaultAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func showTostMsg(title: String?, message: String) {
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        self.present(alertController, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            alertController.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func showTostMsg(title: String?, message: String , completion:@escaping (() -> ()) ) {
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        self.present(alertController, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            alertController.dismiss(animated: true, completion: nil)
            completion()
        }
    }
    
    func rediretToURL(redirectURL : String) {
            if !redirectURL.isEmpty {
                guard let url = URL(string: redirectURL) else { return }
                UIApplication.shared.open(url)
            }else{
                self.showTostMsg(title: "", message: "Redirection URL not found.")
            }
        }
    
    
    /// Set transparent navigation bar
    internal func setTransparentNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    /// Remove transparent navigation bar
    internal func removeTransparentNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    /// Creates custom back butotn on ViewController. Hides default back button and createa a UIBarButtonItem instance on controller and sets to leftBartButtonItem property.
    ///
    /// - Parameter imageName: Name of the image to show. if nil is supplied "picture_done" is assumed, specific to Imaginamos
//     func setupTabNavigation() {
//
//        let leftBarButtonItem1 = UIBarButtonItem(image:  #imageLiteral(resourceName: "address") , style: .plain, target: self , action: #selector(locationBarButtonAction))
//
//        var currentCity = TBUserManager.share.currentCity != nil ? TBUserManager.share.currentCity : TBUserManager.share.selectedLocation.name
//        currentCity = currentCity!.isEmpty ? TBUserManager.share.selectedLocation.name : currentCity
//        let leftBarButtonItem2 :UIBarButtonItem = UIBarButtonItem( title: currentCity, style: .plain, target: self, action: #selector(locationBarButtonAction))
//        let leftBarButtonItem3 :UIBarButtonItem = UIBarButtonItem(image:  #imageLiteral(resourceName: "edit") , style: .plain, target: self , action: #selector(locationBarButtonAction))
//        self.navigationItem.setLeftBarButtonItems([leftBarButtonItem1, leftBarButtonItem2, leftBarButtonItem3], animated: true)
//
//
//
//        let notificationBarButtonItem = UIBarButtonItem(image:  #imageLiteral(resourceName: "bell") , style: .plain, target: self , action: #selector(notificationButton))
//        let searchBarButtonItem:UIBarButtonItem = UIBarButtonItem(image:  #imageLiteral(resourceName: "search") , style: .plain, target: self , action: #selector(searchBarButtonAction))
//        self.navigationItem.setRightBarButtonItems([notificationBarButtonItem, searchBarButtonItem], animated: true)
//        
//    }
    
    func setupBackButton() {
        let backBarButtonItem = UIBarButtonItem(image:  #imageLiteral(resourceName: "Back icon") , style: .plain, target: self , action: #selector(navigateToBackScreen))
        self.navigationItem.setLeftBarButton(backBarButtonItem, animated: true)
        
    }
    
   /// Go back to previsous screen. If pushed if pops else dismisses.
    @objc internal func navigateToBackScreen() {
        if isModal {
            dismiss(animated: true, completion: nil)
        } else {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    var isModal: Bool {
        if presentingViewController != nil {
            return true
        }
        
        if presentingViewController?.presentedViewController == self {
            return true
        }
        
        if navigationController?.presentingViewController?.presentedViewController == navigationController {
            return true
        }
        
        if tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        
        return false
    }
    
    class var storyboardID : String {
        
        return "\(self)"
    }
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
    
    
}

enum AppStoryboard : String {
    
    case Main
    case Home
    case Wishlist
    case Cart
    case Account
    case Categories
    case TabBarManager

    var instance : UIStoryboard {
        
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type, function : String = #function, line : Int = #line, file : String = #file) -> T {
        
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
        }
        
        return scene
    }
    
    func initialViewController() -> UIViewController? {
        
        return instance.instantiateInitialViewController()
    }
}

