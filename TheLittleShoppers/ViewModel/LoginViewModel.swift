//
//  LoginViewModel.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 14/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation

class LoginViewModel{
    var email:String = ""
    var password:String = ""
    
    func loginApiCall(_ completion: @escaping ((JSONDictionary?,ErrorHandler?) -> ())){
        APIController.makeRequestReturnJSON(.login(email: email, password: password)) { (data, error) in
            if error==nil{
                completion(data,nil)
            }else{
                completion(nil,error)
            }
        }
    }
}
