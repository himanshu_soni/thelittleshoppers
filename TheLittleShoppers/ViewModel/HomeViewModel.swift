//
//  HomeViewModel.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 16/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation

class HomeViewModel{
    var userId:Int = 0
    var homeObject:HomeModel?
    
    func getHomeData(_ completion: @escaping((JSONDictionary?,ErrorHandler?) -> ())){
        APIController.makeRequestReturnJSON(.homeApi(userId: 130)) { (data, error) in
            if error == nil{
                self.homeObject = HomeModel(json: data as! [String:Any])
                completion(data,nil)
            }else{
                completion(nil,error)
            }
        }
    }
    
    
    func getHomeData1(_ completion: @escaping((JSONDictionary?,ErrorHandler?) -> ())){
        APIController.makeRequestReturnJSON(.homeApiWithoutuserId) { (data, error) in
            if error == nil{
                self.homeObject = HomeModel(json: data as! [String:Any])
                completion(data,nil)
            }else{
                completion(nil,error)
            }
        }
    }
    
    
}
