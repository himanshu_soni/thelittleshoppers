//
//  CartViewModel.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 17/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation

class CartViewModel:NSObject{
    
    var userId: String = ""
    var cartData: CartModel?
    func getCartData(_ completion : @escaping((JSONDictionary?,ErrorHandler?)->())){
        APIController.makeRequestReturnJSON(.cartList(userId: 139)) { (data, error) in
            
        
              if error==nil{
                  self.cartData = CartModel(json: data!)
                  completion(data,nil)
        
                print(data!)
              }else{
                  completion(nil,error)
              }
          }
      }
}
