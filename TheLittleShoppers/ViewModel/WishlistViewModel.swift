//
//  WishlistViewModel.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 15/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation

class WishlistViewModel:NSObject{
    var userId:String = ""
    var pageCount:String = ""
    
    var wishlistData:WishlistModel?
    func getWishlistData(_ completion : @escaping((JSONDictionary?,ErrorHandler?)->())){
        APIController.makeRequestReturnJSON(.wishList(userId: 130, pageCount: 1)) { (data, error) in
            if error==nil{
                self.wishlistData = WishlistModel(json: data!)
                completion(data,nil)
            }else{
                completion(nil,error)
            }
        }
    }
    
    
    
    
    
}
