//
//  AccountDetailsTableViewCell.swift
//  TestProject
//
//  Created by Apple on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class AccountDetailsTableViewCell: UITableViewCell
{

    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
