//
//  OtherTableViewCell.swift
//  TestProject
//
//  Created by Apple on 22/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class OtherTableViewCell: UITableViewCell
{

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
