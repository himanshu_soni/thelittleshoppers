//
//  ItemsTableViewCell.swift
//  TestProject
//
//  Created by Apple on 22/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Foundation

class ItemsTableViewCell: UITableViewCell
{
    @IBOutlet weak var motherCareTblView: UITableView!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
     var parent = UIViewController()
     var arrData:[CategoryMain]?
     var items:[Category]?
     var vegItems:[VegetablesDetails]?
    
    @IBOutlet weak var btnObj: UIButton!
    @IBOutlet weak var categoryLbl: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        motherCareTblView.isHidden = true
        motherCareTblView.delegate = self
        motherCareTblView.dataSource = self
        self.motherCareTblView.estimatedRowHeight = self.motherCareTblView.rowHeight
        self.motherCareTblView.rowHeight = UITableView.automaticDimension
        
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func btnAction(_ sender: Any)
    {
        if isHidden == true
        {
            btnObj.setImage(UIImage(named: "Plus"), for: .selected)
        }
        else
        {
         btnObj.setImage(UIImage(named: "Minus"), for: .normal)
        }
       
        motherCareTblView.isHidden.toggle()
    }
    
}

extension ItemsTableViewCell:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       return items!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherTableViewCell", for: indexPath)as! OtherTableViewCell
        cell.contentView.backgroundColor = UIColor.white
        cell.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowRadius = 1

        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false;
        cell.clipsToBounds = false;
        //let data = arrData![indexPath.section]
        let img = items![indexPath.row].imgUrl
        let name = items![indexPath.row].motherCareItems
       
       
        cell.imgView.image = UIImage(named: "\(img)")
        cell.nameLbl.text = name
//        let height = motherCareTblView.contentSize.height
//        heightConstraint.constant = height
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let controller = storyboard.instantiateViewController(withIdentifier: "NextViewController") as! NextViewController
        
        controller.arrData = arrData
        controller.items = arrData![indexPath.row].items
        let items = arrData![indexPath.row].items
        controller.vegItems = items[indexPath.section].vegItems
        
        self.parent.navigationController?.pushViewController(controller, animated: true)

    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       return UITableView.automaticDimension
    }
    
    
}
