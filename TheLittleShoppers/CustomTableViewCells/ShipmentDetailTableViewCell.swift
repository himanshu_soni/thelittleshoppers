//
//  ShipmentDetailTableViewCell.swift
//  TestProject
//
//  Created by Pushkar Raj Chauhan on 13/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ShipmentDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var itemMarkLabel:UILabel!
    @IBOutlet weak var quantityLabel:UILabel!
    @IBOutlet weak var discountLabel:UILabel!
    @IBOutlet weak var previewImageView:UIImageView!
    @IBOutlet weak var realPriceLabel:UILabel!
    @IBOutlet weak var cancelButton:UIButton!
    var cancelButtonPressed:(() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func cancelButtonTapped(sender:UIButton){
        self.cancelButtonPressed!()
    }

}
