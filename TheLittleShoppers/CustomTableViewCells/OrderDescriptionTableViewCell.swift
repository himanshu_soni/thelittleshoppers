//
//  OrderDescriptionTableViewCell.swift
//  TestProject
//
//  Created by Pushkar Raj Chauhan on 12/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class OrderDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var previewImageView:UIImageView!
    @IBOutlet weak var orderIdLabel:UILabel!
    @IBOutlet weak var realPrice:UILabel!
    @IBOutlet weak var quantityLabel:UILabel!
    @IBOutlet weak var discountPrice:UILabel!
    @IBOutlet weak var viewYourOrderButton:UIButton!
    var clickOnViewOrder:(()->())?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func viewYourOrderAction(sender:UIButton){
        self.clickOnViewOrder!()
    }

}
