//
//  CartTableViewCell.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 17/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTotalRate: UILabel!
    @IBOutlet weak var lblProductRate: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnDeletProduct(_ sender: Any) {
    }
}
