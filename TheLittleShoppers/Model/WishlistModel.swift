//
//  WishlistModel.swift
//  TestProject
//
//  Created by Pushkar Raj Chauhan on 13/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


class WishlistModel:NSObject{
    
    var message:String = ""
    var status:String = ""
    var wishlistData = [WishlistData]()
    
    convenience init(json Response:[String:Any]){
        self.init()
        self.message = Response["message"] as? String ?? ""
        self.status = Response["status"] as? String ?? ""
        if let tempArray = Response["data"] as? NSArray{
            self.wishlistData = tempArray.map({ (item) -> WishlistData in
                WishlistData.init(json: item as! [String:Any])
            })
         }
    }
}
class WishlistData:NSObject{
    var productId:String = ""
    var productCode:String = ""
    var addedBy:String = ""
    var categoryId:String = ""
    var subCategoryId:String = ""
    var brandId:String = ""
    var productShortName:String = ""
    var productName:String = ""
    var salePrice:String = ""
    var discountedSalePrice:String = ""
    var regularPrice:String = ""
    var genderr:String = ""
    var hsnCode:String = ""
    var mRP:String = ""
    var shortDescription:String = ""
    var descripTion:String = ""
    var stock:String = ""
    var status:String = ""
    var image:String = ""
    var discount:String = ""
    var taxType:String = ""
    var taxPercentage:String = ""
    var barCode:String = ""
    var barcodeImage:String = ""
    var freeUnit:String = ""
    var stateGst:String = ""
    var CGst:String = ""
    var lotNo:String = ""
    var systemGenerateLotNo:String = ""
    var totalBasicPrice:String = ""
    var totalSalePrice:String = ""
    var totalMrp:String = ""
    var seasonId:String = ""
    var createdAt:String = ""
    var wishlistId:String = ""
    var userId:String = ""
    convenience init(json Response:[String:Any]){
        self.init()
        self.productId = Response["product_id"] as? String ?? ""
        self.productCode = Response["product_code"] as? String ?? ""
        self.addedBy = Response["added_by"] as? String ?? ""
        self.categoryId = Response["Category_id"] as? String ?? ""
        self.subCategoryId = Response["subCategory_id"] as? String ?? ""
        self.brandId = Response["brand_id"] as? String ?? ""
        self.productShortName = Response["product_short_name"] as? String ?? ""
        self.productName = Response["product_name"] as? String ?? ""
        self.salePrice = Response["sale_price"] as? String ?? ""
        self.discountedSalePrice = Response["discounted_sale_price"] as? String ?? ""
        self.regularPrice = Response["regular_price"] as? String ?? ""
        self.genderr = Response["gender"] as? String ?? ""
        self.hsnCode = Response["hsn"] as? String ?? ""
        self.mRP = Response["MRP"] as? String ?? ""
        self.shortDescription = Response["short_description"] as? String ?? ""
        self.descripTion = Response["description"] as? String ?? ""
        self.stock = Response["stock"] as? String ?? ""
        self.status = Response["status"] as? String ?? ""
        self.image = Response["image"] as? String ?? ""
        self.discount = Response["discount"] as? String ?? ""
        self.taxType = Response["tax_type"] as? String ?? ""
        self.taxPercentage = Response["tax_percentage"] as? String ?? ""
        self.barCode = Response["barcode"] as? String ?? ""
        self.barcodeImage = Response["barcode_image"] as? String ?? ""
        self.freeUnit = Response["free_unit"] as? String ?? ""
        self.stateGst = Response["s_gst"] as? String ?? ""
        self.CGst = Response["c_gst"] as? String ?? ""
        self.lotNo = Response["lot_no"] as? String ?? ""
        self.systemGenerateLotNo = Response["system_genrateLotno"] as? String ?? ""
        self.totalBasicPrice = Response["totalBasicPrice"] as? String ?? ""
        self.totalSalePrice = Response["totalSalePrice"] as? String ?? ""
        self.totalMrp = Response["totalMRP"] as? String ?? ""
        self.seasonId = Response["season_id"] as? String ?? ""
        self.createdAt = Response["create"] as? String ?? ""
        self.wishlistId = Response["wishlist_id"] as? String ?? ""
        self.userId = Response["user_id"] as? String ?? ""
    }
}


