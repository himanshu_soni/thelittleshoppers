//
//  OrderDescriptionModel.swift
//  TestProject
//
//  Created by Pushkar Raj Chauhan on 12/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class OrderDescriptionModel:NSObject{
    
    
    var orderId:String = ""
    var orderNo:String = ""
    var paymentType:String = ""
    var orderStatus:String = ""
    var orderDate:String = ""
    var totalItemPrice:String = ""
    var grandTotal:String = ""
    var discount:String = ""
    var addressId:String = ""
    var fullName:String = ""
    var pincode:String = ""
    var address:String = ""
    var landMark:String = ""
    var city:String = ""
    var state:String = ""
    var orderedProducts = [OrderedProducts]()
    
    convenience init(json Response:[String:Any]){
        self.init()
        self.orderId = Response["order_id"] as? String ?? ""
        self.orderNo = Response["order_no"] as? String ?? ""
        self.paymentType = Response["payment_type"] as? String ?? ""
        self.orderStatus = Response["order_status"] as? String ?? ""
        self.orderDate = Response["ordered_date"] as? String ?? ""
        self.totalItemPrice = Response["total_item_price"] as? String ?? ""
        self.grandTotal = Response["grand_total"] as? String ?? ""
        self.discount = Response["discount"] as? String ?? ""
        self.addressId = Response["address_id"] as? String ?? ""
        self.address = Response["full_address"] as? String ?? ""
        self.fullName = Response["full_name"] as? String ?? ""
        self.pincode = Response["pincode"] as? String ?? ""
        self.landMark = Response["landmark"] as? String ?? ""
        self.city = Response["city"] as? String ?? ""
        self.state = Response["state"] as? String ?? ""
        if let tempVar = Response["ordered_products"] as? NSArray{
            self.orderedProducts = tempVar.map({ (item) -> OrderedProducts in
                OrderedProducts.init(json: item as! [String : Any])
            })
        }
    }
    
}

class OrderedProducts:NSObject{
    var imageUrl:String = ""
    var productName:String = ""
    var quantity:String = ""
    var discountPrice:String = ""
    var realPrice:String = ""
    
    convenience init(json response:[String:Any]){
        self.init()
    }
}

