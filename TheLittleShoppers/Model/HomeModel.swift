//
//  HomeModel.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 16/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation

class HomeModel:NSObject{
    var message:String = ""
    var status:String = ""
    var homeData:HomeData?
    convenience init(json Response:[String:Any]){
        self.init()
        self.message = Response["message"] as? String ?? ""
        self.status = Response["status"] as? String ?? ""
        if let homeData = Response["data"] as? [String:Any]{
            self.homeData = HomeData.init(json: homeData)
        }
    }
}
class HomeData:NSObject{
    var pagerData = [PagerData]()
    var dealOftheDay:DealOfDay?
    var shopBycategory = [ShopByCategory]()
    var allHomeBannerData = [AllHomeBannerData]()
    var bloomStyleData = [BloomStyleData]()
    var trendingData:Trending?
    var latestProductData = [LatestProductData]()
//    var favouritesData = [FavouritesData]()
    
    convenience init(json Response:[String:Any]){
        self.init()
        if let pagerArray = Response["pagerData"] as? NSArray{
            self.pagerData = pagerArray.map({ (item) -> PagerData in
                PagerData.init(json: item as! [String:Any])
            })
        }
        self.dealOftheDay = DealOfDay.init(json: Response["dealofDay"] as! [String:Any])
        if let shopByCategoryArray = Response["shopByCategory"] as? NSArray{
            self.shopBycategory = shopByCategoryArray.map({ (item) -> ShopByCategory in
                ShopByCategory.init(json: item as! [String:Any])
            })
        }
        if let allHomeBannerData = Response["allHomeBannerData"] as? NSArray{
            self.allHomeBannerData = allHomeBannerData.map({ (item) -> AllHomeBannerData in
                AllHomeBannerData.init(json: item as! [String:Any])
            })
        }
        if let bloomStyleData = Response["bloomStyleData"] as? NSArray{
            self.bloomStyleData = bloomStyleData.map({ (item) -> BloomStyleData in
                BloomStyleData.init(json: item as! [String:Any])
            })
        }
        self.trendingData = Trending.init(json: Response["trendingData"] as! [String:Any])
        if let latestProductsData = Response["latestProductData"] as? NSArray{
            self.latestProductData = latestProductsData.map({ (item) -> LatestProductData in
                LatestProductData.init(json: item as! [String:Any])
            })
        }
//        if let favouritesData = Response["favouriteData"] as? NSArray{
//            self.favouritesData = favouritesData.map({ (item) -> FavouritesData in
//                FavouritesData.init(json: item as! [String:Any])
//            })
//        }
    }
}
//MARK:Pager Data
class PagerData:NSObject{
    var pageBannerId:String = ""
    var bannerImageUrl:String = ""
    var bannerName:String = ""
    var subCategoryId:String = ""
    var createdAt:String = ""
    convenience init(json Response:[String:Any]){
        self.init()
        self.pageBannerId = Response["page_banner_id"] as? String ?? ""
        self.bannerImageUrl = Response["banner"] as? String ?? ""
        self.bannerName = Response["banner_name"] as? String ?? ""
        self.subCategoryId = Response["subcategory_id"] as? String ?? ""
        self.createdAt = Response["created"] as? String ?? ""
    }
}

//MARK:Deal Of the Day Model
class DealOfDay:NSObject{
    var subCategoryId:String = ""
    var dealOfDayData = [DealOfDayData]()
    convenience init(json Response:[String:Any]){
        self.init()
        self.subCategoryId = Response["subcategory_id"] as? String ?? ""
        if let dealArray = Response["data"] as? NSArray{
            self.dealOfDayData = dealArray.map({ (item) -> DealOfDayData in
                DealOfDayData.init(json: item as! [String:Any])
            })
        }
    }
}
class DealOfDayData:NSObject{
    var image:String = ""
    var productName:String = ""
    var productDescription:String = ""
    
    convenience init(json Response:[String:Any]){
        self.init()
        self.image = Response["image"] as? String ?? ""
        self.productName = Response["productName"] as? String ?? ""
        self.productDescription = Response["productDescription"] as? String ?? ""
    }
}

class ShopByCategory:NSObject{
    var subCategoryId:String = ""
    var subcategoryName:String = ""
    var subcategoryImage:String = ""
    var categoryId:String = ""
    convenience init(json Response:[String:Any]){
        self.init()
        self.subCategoryId = Response["subcategory_id"] as? String ?? ""
        self.subcategoryName = Response["subcategory_name"] as? String ?? ""
        self.subcategoryImage = Response["subcategory_image"] as? String ?? ""
        self.categoryId = Response["category_id"] as? String ?? ""
    }
}
class AllHomeBannerData:NSObject{
    var bannerId:String = ""
    var bannerName:String = ""
    var bannerImage:String = ""
    var subcategoryId:String = ""
    var createdAt:String = ""
    
    convenience init(json Response:[String:Any]){
        self.init()
        self.bannerId = Response["app_home_banner_id"] as? String ?? ""
        self.bannerName = Response["banner_name"] as? String ?? ""
        self.bannerImage = Response["banner_image"] as? String ?? ""
        self.subcategoryId = Response["subcategory_id"] as? String ?? ""
        self.createdAt = Response["created"] as? String ?? ""
    }
}
class BloomStyleData:NSObject{
    var subCategoryId:String = ""
    var SubcategoryName:String = ""
    var subCategoryImage:String = ""
    var categoryId:String = ""
    convenience init(json Response:[String:Any]){
        self.init()
        self.subCategoryId = Response["subcategory_id"] as? String ?? ""
        self.SubcategoryName = Response["subcategory_name"] as? String ?? ""
        self.subCategoryImage = Response["subcategory_image"] as? String ?? ""
        self.categoryId = Response["category_id"] as? String ?? ""
    }
}
class Trending:NSObject{
    var subCategoryId:String = ""
    var trendingData = [DealOfDayData]()
    convenience init(json Response:[String:Any]){
        self.init()
        self.subCategoryId = Response["subcategory_id"] as? String ?? ""
        if let trendingData = Response["data"] as? NSArray{
            self.trendingData = trendingData.map({ (item) -> DealOfDayData in
                DealOfDayData.init(json: item as! [String:Any])
            })
        }
    }
}
class LatestProductData:NSObject{
    var productId:String = ""
    var productCode:String = ""
    var addedBy:String = ""
    var categoryId:String = ""
    var subCategoryId:String = ""
    var brandId:String = ""
    var productShortName:String = ""
    var productName:String = ""
    var salePrice:String = ""
    var discountedSalePrice:String = ""
    var regularPrice:String = ""
    var genderr:String = ""
    var hsnCode:String = ""
    var mRP:String = ""
    var shortDescription:String = ""
    var descripTion:String = ""
    var stock:String = ""
    var status:String = ""
    var image:String = ""
    var discount:String = ""
    var taxType:String = ""
    var taxPercentage:String = ""
    var barCode:String = ""
    var barcodeImage:String = ""
    var freeUnit:String = ""
    var stateGst:String = ""
    var CGst:String = ""
    var lotNo:String = ""
    var systemGenerateLotNo:String = ""
    var totalBasicPrice:String = ""
    var totalSalePrice:String = ""
    var totalMrp:String = ""
    var seasonId:String = ""
    var createdAt:String = ""
    convenience init(json Response:[String:Any]) {
        self.init()
        self.productId = Response["product_id"] as? String ?? ""
        self.productCode = Response["product_code"] as? String ?? ""
        self.addedBy = Response["added_by"] as? String ?? ""
        self.categoryId = Response["Category_id"] as? String ?? ""
        self.subCategoryId = Response["subCategory_id"] as? String ?? ""
        self.brandId = Response["brand_id"] as? String ?? ""
        self.productShortName = Response["product_short_name"] as? String ?? ""
        self.productName = Response["product_name"] as? String ?? ""
        self.salePrice = Response["sale_price"] as? String ?? ""
        self.discountedSalePrice = Response["discounted_sale_price"] as? String ?? ""
        self.regularPrice = Response["regular_price"] as? String ?? ""
        self.genderr = Response["gender"] as? String ?? ""
        self.hsnCode = Response["hsn"] as? String ?? ""
        self.mRP = Response["MRP"] as? String ?? ""
        self.shortDescription = Response["short_description"] as? String ?? ""
        self.descripTion = Response["description"] as? String ?? ""
        self.stock = Response["stock"] as? String ?? ""
        self.status = Response["status"] as? String ?? ""
        self.image = Response["image"] as? String ?? ""
        self.discount = Response["discount"] as? String ?? ""
        self.taxType = Response["tax_type"] as? String ?? ""
        self.taxPercentage = Response["tax_percentage"] as? String ?? ""
        self.barCode = Response["barcode"] as? String ?? ""
        self.barcodeImage = Response["barcode_image"] as? String ?? ""
        self.freeUnit = Response["free_unit"] as? String ?? ""
        self.stateGst = Response["s_gst"] as? String ?? ""
        self.CGst = Response["c_gst"] as? String ?? ""
        self.lotNo = Response["lot_no"] as? String ?? ""
        self.systemGenerateLotNo = Response["system_genrateLotno"] as? String ?? ""
        self.totalBasicPrice = Response["totalBasicPrice"] as? String ?? ""
        self.totalSalePrice = Response["totalSalePrice"] as? String ?? ""
        self.totalMrp = Response["totalMRP"] as? String ?? ""
        self.seasonId = Response["season_id"] as? String ?? ""
        self.createdAt = Response["create"] as? String ?? ""
    }
}
