//
//  CartModel.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 17/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation

class CartModel: NSObject{
    
    var message:String = ""
    var status:String = ""
    var cartData = [CartData]()
    
    convenience init(json Response:[String:Any]){
           self.init()
           self.message = Response["message"] as? String ?? ""
           self.status = Response["status"] as? String ?? ""
           if let tempArray = Response["data"] as? NSArray{
               self.cartData = tempArray.map({ (item) -> CartData in
                   CartData.init(json: item as! [String:Any])
               })
            }
       }
}

class CartData : NSObject{
    
    var productId:String = ""
    var subCategoryId:String = ""
    var productName:String = ""
    var originalPrice:String = ""
    var originalSalePrice:String = ""
    var price:String = ""
    var salePrice:String = ""
    var originalRegularPrice:String = ""
    var regularPrice:String = ""
    var image:String = ""
    var discount:String = ""
    var cartId:String = ""
    var userId:String = ""
    var quantity:String = ""
    var colorId:String = ""
    var colorName:String = ""
    var sizeId:String = ""
    var sizeNumber:String = ""
    var ageId:String = ""
    var ageName:String = ""
    var stockStatus:String = ""
    var stockQuantuty:String = ""
    convenience  init(json Response:[String:Any]){
        self.init()
        self.productId = Response["product_id"] as? String ?? ""
         self.subCategoryId = Response["subCategory_id"] as? String ?? ""
         self.productName = Response["product_name"] as? String ?? ""
         self.originalPrice = Response["originalPrice"] as? String ?? ""
         self.originalSalePrice = Response["original_sale_price"] as? String ?? ""
         self.price = Response["price"] as? String ?? ""
         self.salePrice = Response["sale_price"] as? String ?? ""
         self.originalRegularPrice = Response["original_regular_price"] as? String ?? ""
         self.regularPrice = Response["regular_price"] as? String ?? ""
         self.image = Response["image"] as? String ?? ""
         self.discount = Response["discount"] as? String ?? ""
         self.cartId = Response["cart_id"] as? String ?? ""
         self.userId = Response["user_id"] as? String ?? ""
         self.quantity = Response["qty"] as? String ?? ""
         self.colorId = Response["color_id"] as? String ?? ""
         self.colorName = Response["color_name"] as? String ?? ""
         self.sizeId = Response["size_id"] as? String ?? ""
         self.sizeNumber = Response["size_number"] as? String ?? ""
         self.ageId = Response["age_id"] as? String ?? ""
         self.ageName = Response["age_name"] as? String ?? ""
        self.stockStatus = Response["stock_status"] as? String ?? ""
        self.stockQuantuty = Response["stock_qty"] as? String ?? ""
        
        
        
    }
}
