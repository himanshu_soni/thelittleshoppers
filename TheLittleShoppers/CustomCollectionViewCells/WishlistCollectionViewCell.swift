//
//  WishlistCollectionViewCell.swift
//  TestProject
//
//  Created by Pushkar Raj Chauhan on 13/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class WishlistCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var previewImageView:UIImageView!
    @IBOutlet weak var itemNameLabel:UILabel!
    @IBOutlet weak var discounpriceLabel:UILabel!
    @IBOutlet weak var realPriceLabel:UILabel!
    @IBOutlet weak var discountPercentageLabel:UILabel!
    override func awakeFromNib() {
    }
}
