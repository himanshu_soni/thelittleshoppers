//
//  ImagesCollectionViewCell.swift
//  TestProject
//
//  Created by Apple on 22/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ImagesCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
}
