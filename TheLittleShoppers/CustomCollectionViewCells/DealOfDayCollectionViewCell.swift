//
//  DealOfDayCollectionViewCell.swift
//  TestProject
//
//  Created by Apple on 06/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class DealOfDayCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var mrpLbl: UILabel!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var discountLbl: UILabel!
}
