//
//  BloomCollectionViewCell.swift
//  TestProject
//
//  Created by Apple on 08/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class BloomCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
}
