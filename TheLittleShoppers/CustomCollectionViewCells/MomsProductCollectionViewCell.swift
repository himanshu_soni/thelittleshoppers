//
//  MomsProductCollectionViewCell.swift
//  TestProject
//
//  Created by Apple on 26/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class MomsProductCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
}
