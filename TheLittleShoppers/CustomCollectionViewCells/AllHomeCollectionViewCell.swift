//
//  AllHomeCollectionViewCell.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 17/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import UIKit

class AllHomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var previewImageView:UIImageView!
    @IBOutlet weak var offerNameLabel:UILabel!
}
