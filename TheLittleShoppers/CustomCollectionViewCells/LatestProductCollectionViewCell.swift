//
//  LatestProductCollectionViewCell.swift
//  TestProject
//
//  Created by Apple on 08/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class LatestProductCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var priceLbl: UILabel!
}
