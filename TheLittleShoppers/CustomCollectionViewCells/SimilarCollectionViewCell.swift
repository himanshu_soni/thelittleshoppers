//
//  SimilarCollectionViewCell.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 21/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import UIKit

class SimilarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var previewImageView:UIImageView!
    @IBOutlet weak var productNamelabel:UILabel!
    var buttonPressed:(()->())?
    

    override func awakeFromNib() {
        
    }
    @IBAction func buttonPressed(sender:UIButton){
        self.buttonPressed!()
    }
}
