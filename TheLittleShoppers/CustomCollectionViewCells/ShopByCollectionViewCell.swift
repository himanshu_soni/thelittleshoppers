//
//  ShopByCollectionViewCell.swift
//  TestProject
//
//  Created by Apple on 08/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ShopByCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
}
