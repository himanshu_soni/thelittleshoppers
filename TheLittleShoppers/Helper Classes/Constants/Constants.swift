//
//  Constants.swift
//  Numeral
//
//  Created by Johnson Ejezie on 03/11/2017.
//  Copyright © 2017 Jinn LLC. All rights reserved.
//

import Foundation
import UIKit

extension Notification.Name {
    static let refreshAllTabs = Notification.Name("RefreshAllTabs")
   
}


struct Constants {
    static let userDefault = UserDefaults.standard
    static let deviceType = "ios"
    static let deviceId = UIDevice.current.identifierForVendor?.uuidString
    
    struct API {
        static let baseURL = "http://thelittleshoppers.com/API/"
        static let imageBaseURL = "https://api.numeral.solutions"
        static let googlePlacesAPIKey = "AIzaSyDxRlHzQo8E8n-nzvvkUPjlQTis2FU4258"
    }
    struct Texts {
        static let errorParsingResponse = "Unknown Error"
    }
    struct APPKey {
        static let OneSignalAppID = "8164aab6-627e-40e3-bbe7-77527a1f1553"
    }
    
    struct PayU {
        static let sha512 = "SHA-512"
        static let merchantKey = "7Egk6mlg"
        static let merchantId = "6559526"
        static let merchantSalt = "zmAz32Y4pr"
        static let successURL = "https://www.payumoney.com/mobileapp/payumoney/success.php"
        static let failedURL = "https://www.payumoney.com/mobileapp/payumoney/failure.php"
        static let merchantHeader = "UzwXGDdjCY/75KMZUuWz558bwyY1uljRItfza2Xi7Ss="
    }
    struct format {
        static let orderDateFormat = "MMM dd,yyyy "
        static let orderTimeFormat = "hh:mm a"
        static let orderRedeemHistory = "dd MMM, hh:mm a "
         static let orderDateTime = "d MMM, hh a"
    }
}


struct Texts {
    static let filters = "Filters"
    static let currentLocation = "Current Location"
    static let ourTaste = "Our Taste"
    static let ourTasteFilterDescription = "Restaurants that fit the personal taste of the whole group! Find restaurants that all of you will enjoy!"
    static let outsideComfortZone = "Outside Your Comfort Zone"
    static let outsideComfortZoneDescription = "Restaurants you might have not chosen: a way for you and your group of friends to discover new tastes."
    static let addFriendsLongText = "Add the friends you’re dining with"
    static let addFriends = "Add friends"
    static let viewProfile = "View Profile"
    static let experienceIt = "Experience it yourself"
    static let rating = "Rating"
    static let searchFriendLongText = "Search by name or phone number"
    static let add = "Add"
    static let HowWasYourMeal = "How was your meal?"
}

struct NotificationCenterKay {
    static let addToCart = "AddToCart"
}




enum AssetsColor {
   case orenge
   case blue
   case placeholderColor
   case tfBorderColor
  
}

extension UIColor {
    
    static func appColor(_ name: AssetsColor) -> UIColor? {
        switch name {
        case .orenge:
            return UIColor(named: "TB_orange")
        case .blue:
            return UIColor(named: "TB_blue")
        case .placeholderColor:
            return UIColor(named: "TB_placeholderColor")
        case .tfBorderColor:
            return UIColor(named: "tf_borderColor")
            
        }
    }
    
    @nonobjc class var backGroundDarkColor: UIColor {
        return UIColor(red: 56.0 / 255.0, green: 36.0 / 255.0, blue: 97.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var backGroundMediumColor: UIColor {
        return UIColor(red: 66.0 / 255.0, green: 32.0 / 255.0, blue: 90.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var backGroundLowColor: UIColor {
      return UIColor(red: 105.0 / 255.0, green: 19.0 / 255.0, blue: 64.0 / 255.0, alpha: 1.0)
    }
}
