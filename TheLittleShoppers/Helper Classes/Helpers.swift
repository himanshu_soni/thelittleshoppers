//
//  Helpers.swift
//  Numeral
//
//  Created by Johnson Ejezie on 28/11/2017.
//  Copyright © 2017 Jinn LLC. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation
import MBProgressHUD
struct Helpers {
    
    
      static func isLocationPermissionGranted() -> Bool {
          guard CLLocationManager.locationServicesEnabled() else { return false }
          return [.authorizedAlways, .authorizedWhenInUse].contains(CLLocationManager.authorizationStatus())
      }
      
    static func alertToEncourageLocationAccessInitially() {
        let alert = UIAlertController(
          title: "IMPORTANT",
          message: "Location access is required.",
          preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow", style: .default, handler: { (alert) -> Void in
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
          } else {
            // Fallback on earlier versions
          }
        }))
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
          while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
          }
          if topController is UIAlertController {
            print ("alert already presented")
            return
          }
          topController.present(alert, animated: true, completion: nil)
        }
      }
    static func saveNotificationState(state:Bool)
    {
        UserDefaults.standard.set(state, forKey: "isNotify")
    }
    static func getNotificationState() -> Bool
    {
        if(UserDefaults.standard.value(forKey: "isNotify") as? Int == 0)
        {
            return false
        }
        else{
            return true
        }
        //return (UserDefaults.standard.value(forKey: "isNotify") as? Bool)!
    }
    
    static func userToken() -> String {
        guard let token = UserDefaults.standard.value(forKey: "Authorization") else {
            return ""
        }
        return token as! String
    }
    
    static func saveUserToken(token:String){
        UserDefaults.standard.set(token, forKey: "Authorization")
        UserDefaults.standard.synchronize()
    }
    
    static func saveUserData(user:Data){
        UserDefaults.standard.set(user, forKey: KUserData)
        UserDefaults.standard.synchronize()
    }

    static func getUserData() -> Data? {
        return UserDefaults.standard.value(forKey: KUserData) as? Data
    }
    
    static func saveDeviceToken(token:String) {
        UserDefaults.standard.set(token, forKey: "devicetoken")
        UserDefaults.standard.synchronize()
    }
    
    static func getDeviceToken() -> String {
        return UserDefaults.standard.value(forKey: "devicetoken") as? String ?? ""
    }
    
    static func removeUserToken() {
        UserDefaults.standard.removeObject(forKey: "Authorization")
    }
    static func removeUser() {
        UserDefaults.standard.removeObject(forKey: KUserData)
    }
    
    
    
    
    static func validateResponse(_ statusCode: Int) -> Bool {
        if case 200...300 = statusCode {
            return true
        }
        return false
    }
    
}

//extension String {
//    var isValidURL: Bool {
//        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
//        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.endIndex.encodedOffset)) {
//            // it is a link, if the match covers the whole string
//            return match.range.length == self.endIndex.encodedOffset
//        } else {
//            return false
//        }
//    }
//}

extension UIButton {
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
    }
}



extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}


func isValidCommercialEmail(emailStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let gmailRegEx = "[A-Z0-9a-z._%+-]+@[gmail]+\\.[A-Za-z]{2,64}"
    let yahoo = "[A-Z0-9a-z._%+-]+@[yahoo]+\\.[A-Za-z]{2,64}"
    let rediffmail = "[A-Z0-9a-z._%+-]+@[rediffmail]+\\.[A-Za-z]{2,64}"
    let yopmail = "[A-Z0-9a-z._%+-]+@[yopmail]+\\.[A-Za-z]{2,64}"
    let outlook = "[A-Z0-9a-z._%+-]+@[outlook]+\\.[A-Za-z]{2,64}"
    
    let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let gmailPred = NSPredicate(format: "SELF MATCHES %@", gmailRegEx)
    let yahooPred = NSPredicate(format: "SELF MATCHES %@", yahoo)
    let rediffPred = NSPredicate(format: "SELF MATCHES %@", rediffmail)
    let yopmailPred = NSPredicate(format: "SELF MATCHES %@", yopmail)
    let outlookPred = NSPredicate(format: "SELF MATCHES %@", outlook)
    
    if(gmailPred.evaluate(with: emailStr) == true || yahooPred.evaluate(with: emailStr) == true || rediffPred.evaluate(with: emailStr) == true || yopmailPred.evaluate(with: emailStr) == true || outlookPred.evaluate(with: emailStr) == true || emailPred.evaluate(with: emailStr) == false) {
        return false
    } else {
        return true
    }
}



extension String {
    func htmlAttributedString() -> NSAttributedString? {
     //    let newFont = UIFont(name: "Poppins-Regular", size: 15)
        
      //  let myAttribute = [ NSAttributedString.Key.foregroundColor: UIColor.blue ]
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
       // html.addAttribute(UIFont(name: "Poppins-Regular", size: 15), value: 15, range: 2000)
        html.setAttributes([NSAttributedString.Key.font : UIFont(name: "Poppins-Regular", size: 15)!], range: NSMakeRange(0, html.length))
        return html
    }
    
    
    
    
    

}
