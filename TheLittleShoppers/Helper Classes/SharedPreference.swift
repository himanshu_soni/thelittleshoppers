//
//  SharedPreference.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 14/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation
import UIKit


let KUserData = "__user_data"
let KWelcomePlayed = "__welcome_played"



class SharedPreference: NSObject {
    
    
    fileprivate let defaults = UserDefaults.standard
    static let sharedInstance = SharedPreference()
    
    class func isKeyPresentInUserDefaults(_ key: String) -> Bool {
        return sharedInstance.isPresent(key)
    }
    fileprivate func isPresent (_ key: String) -> Bool{
        return defaults.object(forKey: key) != nil
    }
 
    
    class var isWelcomePlayed : Bool {
        get{
            return ((UserDefaults.standard.value(forKey: KWelcomePlayed) as? Bool) ?? false)
        }
        set{
            set(newValue, forKey: KWelcomePlayed)
        }
    }
    
    // Sets value in user defaults
    ///
    /// - Parameters:
    ///   - value: value to be saved
    ///   - key: key of value
    class func set(_ value:Any, forKey key: String){
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
//    // UserData
//    class func userData(_ user: Data) {
//        sharedInstance.setUserData(user)
//    }
//    class func getUserData() -> Data {
//        return sharedInstance.getUserData()
//    }
//    fileprivate func setUserData(_ user: Data){
//        defaults.set(user, forKey: KUserData)
//    }
//    fileprivate func getUserData() -> Data {
//        if let data = defaults.value(forKey: KUserData) {
//            return data as! Data
//        }else{
//            return Data()
//        }
//    }
//
//
//
    
    
//    if let data = Helpers.getUserData() {
//        do {
//            if let dict = try JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionary {
//                WaittyUserManager.share.user = User(json: dict)
//
//                let vc = WaittyTabBarController.instantiate(fromAppStoryboard: .dashboard)
//                self.navigationController?.pushViewController(vc, animated: true)
//
//            }
//        } catch {
//            print(error.localizedDescription)
//        }
//    }
    
}
