//
//  AllApi.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 14/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation
import Moya
import Result
import Alamofire

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}
let HimanshuApiProvider = MoyaProvider<SoniApi>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])
enum SoniApi {
    case login(email: String, password: String)
    case wishList(userId:Int,pageCount:Int)
    case homeApiWithoutuserId
    case homeApi(userId:Int)
    case cartList(userId:Int)
}
extension SoniApi: TargetType {
//    public var headers: [String : String]? {
////        switch self {
////        case .facebookAuth:
////            return nil
////        default:
////            return ["Authorization": "\(Helpers.userToken())"]
////        }
//        return ["Authorization": "Bearer \(Helpers.userToken())"]
//
//    }
    
    var baseURL : URL { return URL(string: Constants.API.baseURL)! }
    
    var path: String {
        switch self {
        case .login:
            return "login.php"
        case .wishList(let userId ,let pageCount):
            return "showWishlistByUserId.php?user_id=\(userId)&page_count=\(pageCount)"
        case .homeApi(let userId):
            return "homePageData.php?user_id\(userId)"
        case .cartList(let userId):
            return "showCartListByUserId.php?user_id=\(userId)"
        case .homeApiWithoutuserId:
            return "homePageData.php"
        }
    }
    public var method: Moya.Method {
        switch self {
        case .login :
            return .post
        case .wishList:
            return .get
        case .homeApi:
            return .get
        case .cartList:
            return .post
        case .homeApiWithoutuserId:
            return .get
        }
    }
    var sampleData: Data {
        return "".data(using: .utf8)!
    }
    
    var task: Task {
        switch self {
        case .login(let email,let password):
            return .requestParameters(parameters: ["email": email,"password": password,"deviceToken": Helpers.getDeviceToken() ], encoding: JSONEncoding.default)
        case .wishList,.homeApi, .cartList,.homeApiWithoutuserId:
            return .requestPlain
        }
    }
    struct JsonArrayEncoding: Moya.ParameterEncoding {
        public static var `default`: JsonArrayEncoding { return JsonArrayEncoding() }
        public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
            var req = try urlRequest.asURLRequest()
            let json = try JSONSerialization.data(withJSONObject: parameters!["jsonArray"]!, options: JSONSerialization.WritingOptions.prettyPrinted)
            req.setValue("application/json", forHTTPHeaderField: "Content-Type")
            req.httpBody = json
            return req
        }
    }
}
