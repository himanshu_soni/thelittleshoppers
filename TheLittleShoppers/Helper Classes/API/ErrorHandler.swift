//
//  ErrorHandler.swift
//  TheLittleShoppers
//
//  Created by Pushkar Raj Chauhan on 14/07/20.
//  Copyright © 2020 Himanshu Soni. All rights reserved.
//

import Foundation
struct ErrorHandler: Swift.Error {
    let code: String
    let desc: String
    
    static func failure(_ code: String , _ text: String) -> ErrorHandler {
        return ErrorHandler.init(code: code, desc: text)
    }
    static func noConnectivity() -> ErrorHandler {
        return ErrorHandler.init(code: "111", desc: "No Internet!")
    }
    static func unknownError() -> ErrorHandler {
        return ErrorHandler.init(code: "1000", desc: "Unknown Error")
    }
}
extension ErrorHandler {
    init(_ json: JSONDictionary) {
        self.code = json["code"] as? String ?? ""
        self.desc = json["error"] as? String ?? json["message"] as? String ?? ""
    }
}
